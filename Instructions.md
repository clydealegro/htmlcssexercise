Here are some requirements for the exercise:

1. Width of the body is 1000px.
2. Here are the image links to use
	
	http://goo.gl/66dnI0
	http://goo.gl/geFYP7
	http://goo.gl/TUeFer
	http://goo.gl/y1ag22
	http://goo.gl/khGTL6

3. You just use 5 posts for the markup
4. For the logo just a capital "M" instead
5. Copy the output.png as close as possible
6. Using what you have learned so far, do the markup as semantically correct as possible.
